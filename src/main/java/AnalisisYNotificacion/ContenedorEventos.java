package AnalisisYNotificacion;

import java.util.ArrayList;
import java.util.List;

import Divisa.Evento;

public class ContenedorEventos {
	private List<Evento> eventosParaAnalizar;
	private List<Evento> eventosParaNotificar;
	private List<Evento> eventosCompletados;
	
	public ContenedorEventos() {
		this.eventosParaAnalizar = new ArrayList<Evento>();
		this.eventosParaNotificar = new ArrayList<Evento>();
		this.eventosCompletados = new ArrayList<Evento>();
	}
	
	public void cargarEventos(List<Evento> eventos) {
		for (Evento evento: eventos) {
			if ( !this.eventosParaAnalizar.contains(evento) &&
					!this.eventosParaNotificar.contains(evento) &&
					!this.eventosCompletados.contains(evento)) {
				this.agregarEventoParaAnalizar(evento);
			}
		}
	}
	
	public void agregarEventoParaAnalizar(Evento evento) {
		this.eventosParaAnalizar.add(evento);
	}
	
	public void agregarEventoParaNotificar(Evento evento) {
		this.eventosParaNotificar.add(evento);
	}
	
	public void agregarEventoCompletado(Evento evento) {
		this.eventosCompletados.add(evento);
	}
	
	public void eliminarEventoParaAnalizar(Evento evento) {
		this.eventosParaAnalizar.remove(evento);
	}
	
	/*public void eliminarEventoParaNotificar(Evento evento) {
		this.eventosParaNotificar.remove(evento);
	}*/
	
	/*public void eliminarEventoCompletado(Evento evento) {
		this.eventosCompletados.remove(evento);
	}*/

	public List<Evento> getEventosParaAnalizar() {
		return eventosParaAnalizar;
	}

	public List<Evento> getEventosParaNotificar() {
		return eventosParaNotificar;
	}

	public List<Evento> getEventosCompletados() {
		return eventosCompletados;
	}
}
