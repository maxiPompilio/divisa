package AnalisisYNotificacion;

import java.util.List;
import java.util.Map;

import Divisa.Evento;
import Interfaces.Notificacion;

public class Notificador {

	private List<Notificacion> notificadores;
	
	public Notificador(List<Notificacion> notificadores) {
		this.notificadores = notificadores;
	}
	
	public void notificar(ContenedorEventos ce) {
		for(Evento evento: ce.getEventosParaNotificar()) {
			String mensaje = "@" + evento.getUsuario().getNickname() + " La divisa " + evento.getDivisaInicial().getNombre();
			mensaje = mensaje + " a alcanzado el valor de " + evento.getDivisaActualizada().getValor();
			mensaje = mensaje + " con respecto a la divisa " + evento.getDivisaActualizada().getBase();
			this.notificadores.get(0).notificar(mensaje);
			ce.agregarEventoCompletado(evento);
		}
		ce.getEventosParaNotificar().clear();
	}
}
