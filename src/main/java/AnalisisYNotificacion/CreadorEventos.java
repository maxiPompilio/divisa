package AnalisisYNotificacion;

import java.util.ArrayList;
import java.util.List;

import Divisa.Divisa;
import Divisa.Evento;
import Divisa.Usuario;
import Interfaces.EventoConfigurable;

public class CreadorEventos {

	public List<Evento> crearEventos(List<EventoConfigurable> eventosConfigurables) {
		
		List<Evento> ret = new ArrayList<Evento>();
		
		for(EventoConfigurable ec: eventosConfigurables) {
			
			String id = ec.getEventoId();
			Usuario usuario = new Usuario(ec.getUsuarioId(), ec.getUsuarioNickname(), ec.getTipoPlataforma());
			String tipoEvento = ec.getTipoEvento();
			Double valor = ec.getValor();
			Divisa divisaInicial = new Divisa(ec.getDivisaComparable(), ec.getDivisaBase(), -1.00);
			Divisa divisaActualizada = new Divisa(ec.getDivisaComparable(), ec.getDivisaBase(), -1.00);
			
			Evento evento = new Evento(id, usuario, tipoEvento, valor, divisaInicial, divisaActualizada);
			
			ret.add(evento);
		}
		
		return ret;
	}
}
