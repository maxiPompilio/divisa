package AnalisisYNotificacion;

import java.util.ArrayList;
import java.util.List;

import Divisa.Evento;

public class Analizador {
	
	public void analizarEventos(ContenedorEventos ce, Cotizador cotizador) {
		
		List<Evento> eventosParaNotificar = new ArrayList<Evento>();
		
		for(Evento evento: ce.getEventosParaAnalizar() ) {
			Double valorActual = cotizador.obtenerValorDivisa(evento.getDivisaInicial().getNombre(), evento.getDivisaInicial().getBase());
			if(evento.getDivisaInicial().getValor().equals(-1.00)) {
				evento.getDivisaInicial().setValor(valorActual);
				evento.getDivisaActualizada().setValor(valorActual);
			} else {
				evento.getDivisaActualizada().setValor(valorActual);
			}
			
			Double valorInicial = evento.getDivisaInicial().getValor();
			Double valorActualizada = evento.getDivisaActualizada().getValor();
			
			if(evento.getTipoEvento().equals("objetivo")) {
				if(valorActualizada > evento.getValor()) {
					eventosParaNotificar.add(evento);
				}
			} else if(evento.getTipoEvento().equals("fijo")) {
				if((valorActualizada - valorInicial) > evento.getValor()) {
					eventosParaNotificar.add(evento);
				}
			} else if(evento.getTipoEvento().equals("procentual")) {
				if((valorActualizada) > (valorInicial * (1 + evento.getValor()))) {
					eventosParaNotificar.add(evento);
				}
			}
		}
		
		for(Evento evento: eventosParaNotificar) {
			ce.agregarEventoParaNotificar(evento);
			ce.eliminarEventoParaAnalizar(evento);
		}
	}

}
