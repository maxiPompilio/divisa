package AnalisisYNotificacion;

import com.google.gson.JsonObject;

import Interfaces.APICotizacion;

public class Cotizador {

	private APICotizacion apiCotizacion;
	
	public Cotizador(APICotizacion apiCotizacion) {
		this.apiCotizacion = apiCotizacion;
	}
	
	public Double obtenerValorDivisa(String divisaNombre, String divisaBase) {
		
		JsonObject cotizaciones = this.apiCotizacion.obtenerCotizaciones(divisaBase);
		
		Double ret = 0.00;
		
		if (cotizaciones == null) {
			return -1.00;
		} else {
			try {
				ret = ret = cotizaciones.get(divisaNombre).getAsDouble();
			} catch(NullPointerException e) {
				ret = -1.00;
			}
		}
		
		return ret;
	}
}
