package BusquedaYCarga;

import java.util.ArrayList;
import java.util.List;

import Interfaces.EventoConfigurable;
import Interfaces.Loader;

public class EventoConfigurableLoader implements Loader<EventoConfigurable> {

	@Override
	public List<EventoConfigurable> load(List<Class> clases) {
		
		List<EventoConfigurable> ret = new ArrayList<EventoConfigurable>();
		
		for(Class clase: clases) {
			if(EventoConfigurable.class.isAssignableFrom(clase)) {
				EventoConfigurable e;
				try {
					e = (EventoConfigurable) clase.newInstance();
					ret.add(e);
				} catch (InstantiationException | IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		return ret;
	}
}
