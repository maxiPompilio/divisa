package BusquedaYCarga;

import java.util.ArrayList;
import java.util.List;

import Interfaces.Loader;
import Interfaces.Notificacion;

public class NotificacionLoader implements Loader<Notificacion> {

	@Override
	public List<Notificacion> load(List<Class> clases) {
		List<Notificacion> ret = new ArrayList<Notificacion>();
		
		for(Class clase: clases) {
			if(Notificacion.class.isAssignableFrom(clase)) {
				Notificacion e;
				try {
					e = (Notificacion) clase.newInstance();
					ret.add(e);
				} catch (InstantiationException | IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		return ret;
	}

}
