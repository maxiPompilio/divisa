package BusquedaYCarga;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Buscador {
	
	public List<Class> encontrarClases(String path, String paquete) {
		
		List<Class> ret = new ArrayList<Class>();
		
		for (File f: new File(path).listFiles()) {
			if (f.getName().contains(".class")) {
				String claseNombre = paquete + "." + f.getName().substring(0, f.getName().length()-6);
				Class c;
				try {
					c = Class.forName(claseNombre);
					ret.add(c);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return ret;
	}
}
