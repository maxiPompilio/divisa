package BusquedaYCarga;

import java.util.ArrayList;
import java.util.List;

import Interfaces.APICotizacion;
import Interfaces.Loader;

public class APICotizacionLoader implements Loader<APICotizacion> {

	@Override
	public List<APICotizacion> load(List<Class> clases) {
		List<APICotizacion> ret = new ArrayList<APICotizacion>();
		
		for(Class clase: clases) {
			if(APICotizacion.class.isAssignableFrom(clase)) {
				APICotizacion e;
				try {
					e = (APICotizacion) clase.newInstance();
					ret.add(e);
				} catch (InstantiationException | IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		return ret;
	}

}
