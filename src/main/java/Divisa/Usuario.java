package Divisa;

public class Usuario {
	
	private String id;
	private String nickname;
	private String plataforma;
	
	public Usuario(String id, String nickname, String plataforma) {
		this.id = id;
		this.nickname = nickname;
		this.plataforma = plataforma;
	}

	/*public String getId() {
		return id;
	}*/

	/*public void setId(String id) {
		this.id = id;
	}*/

	public String getNickname() {
		return nickname;
	}

	/*public void setNickname(String nickname) {
		this.nickname = nickname;
	}*/

	public String getPlataforma() {
		return plataforma;
	}

	/*public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}*/
	
	/*@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Usuario usuario = (Usuario) obj;
		
		return this.id.equals(usuario.id)
				&& this.nickname.equals(usuario.nickname)
				&& this.plataforma.equals(usuario.plataforma);
	}*/
}
