package Divisa;

public class Evento {

	private String id;
	private Usuario usuario;
	private String tipoEvento;
	private Double valor;
	private Divisa divisaInicial;
	private Divisa divisaActualizada;
	
	public Evento(String id, Usuario usuario, String tipoEvento, Double valor, Divisa divisaInicial, Divisa divisaActualizada) {
		this.id = id;
		this.usuario = usuario;
		this.tipoEvento = tipoEvento;
		this.valor = valor;
		this.divisaInicial = divisaInicial;
		this.divisaActualizada = divisaActualizada;
	}
	
	/*public String getId() {
		return id;
	}*/
	
	/*public void setId(String id) {
		this.id = id;
	}*/

	public Usuario getUsuario() {
		return usuario;
	}

	/*public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}*/

	public String getTipoEvento() {
		return tipoEvento;
	}

	/*public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}*/

	public Double getValor() {
		return valor;
	}

	/*public void setValor(Double valor) {
		this.valor = valor;
	}*/

	public Divisa getDivisaInicial() {
		return divisaInicial;
	}

	/*public void setDivisaInicial(Divisa divisaInicial) {
		this.divisaInicial = divisaInicial;
	}*/

	public Divisa getDivisaActualizada() {
		return divisaActualizada;
	}

	/*public void setDivisaActualizada(Divisa divisaActualizada) {
		this.divisaActualizada = divisaActualizada;
	}*/
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Evento evento = (Evento) obj;
		
		return this.id.equals(evento.id);
	}
	
	/*@Override
	public String toString() {
		return "IdEvento: " + this.id;
	}*/
}
