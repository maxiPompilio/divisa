package Divisa;

public class Divisa {

	private String nombre;
	private String base;
	private Double valor;
	
	public Divisa(String nombre, String base, Double valor) {
		this.nombre = nombre;
		this.base = base;
		this.valor = valor;
	}
	
	public String getNombre() {
		return nombre;
	}
	/*public void setNombre(String nombre) {
		this.nombre = nombre;
	}*/
	public String getBase() {
		return base;
	}
	/*public void setBase(String base) {
		this.base = base;
	}*/
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}

	/*@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Divisa divisa = (Divisa) obj;
		
		return this.nombre.equals(divisa.nombre)
				&& this.base.equals(divisa.base)
				&& this.valor.equals(divisa.valor);
	}*/
	
	
}
