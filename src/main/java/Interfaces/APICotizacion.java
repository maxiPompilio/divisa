package Interfaces;

import com.google.gson.JsonObject;

public interface APICotizacion {

	public JsonObject obtenerCotizaciones(String divisaNombre);
}
