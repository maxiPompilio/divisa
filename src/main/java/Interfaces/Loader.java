package Interfaces;

import java.util.List;

public interface Loader<T> {

	public List<T> load(List<Class> clases);
}
