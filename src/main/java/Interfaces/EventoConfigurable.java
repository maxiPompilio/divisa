package Interfaces;

public interface EventoConfigurable {

	public String getEventoId();
	public String getUsuarioId();
	public String getUsuarioNickname();
	public String getTipoPlataforma();
	public String getDivisaBase();
	public String getDivisaComparable();
	public String getTipoEvento();
	public Double getValor();
	
}
