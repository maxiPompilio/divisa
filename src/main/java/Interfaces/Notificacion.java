package Interfaces;

public interface Notificacion {

	public boolean notificar(String mensaje);
}
