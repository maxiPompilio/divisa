package Divisa;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import AnalisisYNotificacion.Analizador;
import AnalisisYNotificacion.ContenedorEventos;
import AnalisisYNotificacion.Cotizador;
import AnalisisYNotificacion.CreadorEventos;
import AnalisisYNotificacion.Notificador;
import BusquedaYCarga.APICotizacionLoader;
import BusquedaYCarga.Buscador;
import BusquedaYCarga.EventoConfigurableLoader;
import BusquedaYCarga.NotificacionLoader;
import Interfaces.APICotizacion;
import Interfaces.EventoConfigurable;
import Interfaces.Notificacion;

public class Tests {
	
	static BateriaEventos bateriaEventos = new BateriaEventos();
	
	@Test public void testCotizador() {
		
		//Existen 3 archivos .class en el directorio elejido
    	Buscador buscador = new Buscador();
    	List<Class> clases = buscador.encontrarClases("externalProjects/APICotizacionProject/APICotizacionProject", "APICotizacionProject");
    	assertTrue(clases.size() == 3);
    	
    	//Cargo las 3 clases que implementan la interfaz APICotizacion
    	APICotizacionLoader acl = new APICotizacionLoader();
    	List<APICotizacion> apiCotizacionesImpl = acl.load(clases);
    	assertTrue(apiCotizacionesImpl.size() == 3);
    	
    	//Creo un cotizador y utilizo la implementacion que consulta los valores de las divisas de forma real
    	Cotizador cotizador = new Cotizador(apiCotizacionesImpl.get(1));
    	
    	//Consultamos los resultados al dia de la fecha 13/12/2020
    	assertTrue(cotizador.obtenerValorDivisa("ARS", "USD") == 82.0659);
    	assertTrue(cotizador.obtenerValorDivisa("ARS", "EUR") == 99.4592);
    	assertTrue(cotizador.obtenerValorDivisa("ARS", "BRL") == 16.2652);
    	assertTrue(cotizador.obtenerValorDivisa("EUR", "USD") == 0.8232);
    	
    	//Consultamos resultados erroneos, cuando se tipea de manera erronea una divisa devuelve -1
    	
    	assertTrue(cotizador.obtenerValorDivisa("ARS", "USDD") == -1.00);
    	assertTrue(cotizador.obtenerValorDivisa("ARSS", "EUR") == -1.00);
    	assertTrue(cotizador.obtenerValorDivisa("ARSSS", "BRLLL") == -1.00);
    	assertTrue(cotizador.obtenerValorDivisa("TTT", "XXX") == -1.00);
    }
	
	@Test public void testNotificador() {
    	
		//Existe un unico archivo .class en el directorio elejido
    	Buscador buscador = new Buscador();
    	List<Class> clases = buscador.encontrarClases("externalProjects/NotificacionProject/NotificacionProject", "NotificacionProject");
    	assertTrue(clases.size() == 1);
    	
    	//Cargo la clase encontrada que implementa la interfaz Notificacion
    	NotificacionLoader nl = new NotificacionLoader();
    	List<Notificacion> notificadores = nl.load(clases);
    	assertTrue(notificadores.size() == 1);
    	
    	//Se testea de manera individual el uso de la clase implementada del proyecto externo
    	assertTrue(notificadores.get(0).notificar("Probando Implementacion"));
    	
    	//No se permite notificar un mensaje que ya fue notificado anteriormente
    	assertFalse(notificadores.get(0).notificar("Probando Implementacion"));
    	
    	//Se crea un ContenedorEventos y se cargan eventos de prueba para notificar
    	ContenedorEventos ce = new ContenedorEventos();
    	ce.agregarEventoParaNotificar(bateriaEventos.eventosMock.get(0));
    	ce.agregarEventoParaNotificar(bateriaEventos.eventosMock.get(1));
    	ce.agregarEventoParaNotificar(bateriaEventos.eventosMock.get(2));
    	
    	//Se crea un Notificador encargado de realizar las notificaciones
    	Notificador notificador = new Notificador(notificadores);
    	notificador.notificar(ce);
    	
    	//Se comprueba que los eventos cambian de ubicacion de lista al ser notificados
    	assertTrue(ce.getEventosParaNotificar().size() == 0);
    	assertTrue(ce.getEventosCompletados().size() == 3);
    	
    	//Se comprueba que al ejecutarse de vuelta no hay modificaciones
    	notificador.notificar(ce);
    	assertTrue(ce.getEventosParaNotificar().size() == 0);
    	assertTrue(ce.getEventosCompletados().size() == 3);
    	
    	//Se comprueba que se realizaron las notificaciones
    	assertFalse(notificadores.get(0).notificar("@usuarioPrueba1 La divisa ARS a alcanzado el valor de 83.02 con respecto a la divisa USD"));
    	assertFalse(notificadores.get(0).notificar("@usuarioPrueba2 La divisa ARS a alcanzado el valor de 83.56 con respecto a la divisa USD"));
    	assertFalse(notificadores.get(0).notificar("@usuarioPrueba3 La divisa ARS a alcanzado el valor de 84.13 con respecto a la divisa USD"));
    }
	
	@Test public void testAppCiclo() {
		
		//Clases ya tomadas en cuenta como instanciadas
    	Buscador buscador = new Buscador();
    	List<Class> clases2 = buscador.encontrarClases("externalProjects/APICotizacionProject/APICotizacionProject", "APICotizacionProject");
    	APICotizacionLoader acl = new APICotizacionLoader();
    	List<Class> clases3 = buscador.encontrarClases("externalProjects/NotificacionProject/NotificacionProject", "NotificacionProject");
    	NotificacionLoader nl = new NotificacionLoader();
    	ContenedorEventos contenedorEventos = new ContenedorEventos();
    	
    	
    	//Existen 3 archivos .class en el directorio elejido
    	List<Class> clases = buscador.encontrarClases("externalProjects/EventosProject/EventosProject", "EventosProject");
    	assertTrue(clases.size() == 3);
    	
    	//Cargo los archivos encontrados
    	EventoConfigurableLoader ecl = new EventoConfigurableLoader();
    	List<EventoConfigurable> eventosConfigurables = ecl.load(clases);
    	assertTrue(eventosConfigurables.size() == 3);
    	
    	//Modelo las implementaciones encontradas en el proyecto externo EventosProject
    	CreadorEventos creadorEventos = new CreadorEventos();
    	List<Evento> eventosCreados = creadorEventos.crearEventos(eventosConfigurables);
    	assertTrue(eventosCreados.size() == 3);
    	
    	//En distintos Cotizador cargo tanto la implementacion real como las stubs
    	List<APICotizacion> apiCotizaciones = acl.load(clases2);
    	Cotizador cotizadorReal = new Cotizador(apiCotizaciones.get(1));
    	Cotizador cotizadorFake1 = new Cotizador(apiCotizaciones.get(2));
    	Cotizador cotizadorFake2 = new Cotizador(apiCotizaciones.get(0));
    	
    	//Se preparan las clases encargadas de analizar y notificar
    	Analizador analizador = new Analizador();
    	List<Notificacion> notificadores = nl.load(clases3);
    	Notificador notificador = new Notificador(notificadores);
    	
    	
    	//Se almacenan los eventos creados
    	contenedorEventos.cargarEventos(eventosCreados);
    	
    	//Se comprueba el estado de las listas antes del Primer ciclo
    	assertTrue(contenedorEventos.getEventosParaAnalizar().size() == 3);
    	assertTrue(contenedorEventos.getEventosParaNotificar().size() == 0);
    	assertTrue(contenedorEventos.getEventosCompletados().size() == 0);
    	
    	// Ejecucion Primer ciclo
    	analizador.analizarEventos(contenedorEventos, cotizadorReal);
    	assertTrue(contenedorEventos.getEventosParaAnalizar().size() == 3);
    	assertTrue(contenedorEventos.getEventosParaNotificar().size() == 0);
    	assertTrue(contenedorEventos.getEventosCompletados().size() == 0);
    	
    	notificador.notificar(contenedorEventos);
    	assertTrue(contenedorEventos.getEventosParaAnalizar().size() == 3);
    	assertTrue(contenedorEventos.getEventosParaNotificar().size() == 0);
    	assertTrue(contenedorEventos.getEventosCompletados().size() == 0);
    	
    	
    	// Ejecucion Segundo ciclo
    	analizador.analizarEventos(contenedorEventos, cotizadorFake1);
    	assertTrue(contenedorEventos.getEventosParaAnalizar().size() == 3);
    	assertTrue(contenedorEventos.getEventosParaNotificar().size() == 0);
    	assertTrue(contenedorEventos.getEventosCompletados().size() == 0);
    	
    	notificador.notificar(contenedorEventos);
    	assertTrue(contenedorEventos.getEventosParaAnalizar().size() == 3);
    	assertTrue(contenedorEventos.getEventosParaNotificar().size() == 0);
    	assertTrue(contenedorEventos.getEventosCompletados().size() == 0);
    	
    	// Ejecucion Tercer ciclo
    	analizador.analizarEventos(contenedorEventos, cotizadorFake2);
    	assertTrue(contenedorEventos.getEventosParaAnalizar().size() == 1);
    	assertTrue(contenedorEventos.getEventosParaNotificar().size() == 2);
    	assertTrue(contenedorEventos.getEventosCompletados().size() == 0);
    	
    	notificador.notificar(contenedorEventos);
    	assertTrue(contenedorEventos.getEventosParaAnalizar().size() == 1);
    	assertTrue(contenedorEventos.getEventosParaNotificar().size() == 0);
    	assertTrue(contenedorEventos.getEventosCompletados().size() == 2);
    }
	
}
