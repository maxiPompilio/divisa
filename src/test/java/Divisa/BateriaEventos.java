package Divisa;

import java.util.ArrayList;
import java.util.List;

public class BateriaEventos {

	public List<Evento> eventosMock;
	
	public BateriaEventos() {
		
		Usuario user1 = new Usuario("1", "usuarioPrueba1", "Twitter");
		Usuario user2 = new Usuario("2", "usuarioPrueba2", "Twitter");
		Usuario user3 = new Usuario("3", "usuarioPrueba3", "Twitter");
		Divisa div1 = new Divisa("ARS", "USD", 82.05);
		Divisa div2 = new Divisa("ARS", "USD", 83.02);
		Divisa div3 = new Divisa("ARS", "USD", 83.56);
		Divisa div4 = new Divisa("ARS", "USD", 84.13);
		
		Evento ev1 = new Evento("1", user1, "puntual", 0.50, div1, div2);
		Evento ev2 = new Evento("2", user2, "objetivo", 83.00, div1, div3);
		Evento ev3 = new Evento("3", user3, "objetivo", 84.00, div1, div4);
		
		this.eventosMock = new ArrayList<Evento>();
		
		this.eventosMock.add(ev1);
		this.eventosMock.add(ev2);
		this.eventosMock.add(ev3);
	}
}
